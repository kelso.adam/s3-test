<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Movie
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Actor[] $actors
 * @property-read \App\Entities\User $user
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $year
 * @property int|null $rating
 * @property int|null $genre
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Movie whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Movie whereGenre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Movie whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Movie whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Movie whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Movie whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Movie whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Movie whereYear($value)
 */
class Movie extends Model
{
    protected   $appends = ['modifiable'],
                $hidden = ['user_id'];

    // Computed
    public function getModifiableAttribute()
    {
        return $this->attributes['user_id'] == request()->user()->id;
    }

    // Relations
    public function actors()
    {
        return $this->belongsToMany(Actor::class, 'actors_movies');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
