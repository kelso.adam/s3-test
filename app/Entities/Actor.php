<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Actor
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Movie[] $movies
 * @mixin \Eloquent
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Actor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Actor whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Actor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Actor whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Actor whereUpdatedAt($value)
 */
class Actor extends Model
{
    // Relations
    public function movies()
    {
        return $this->belongsToMany(Movie::class, 'actors_movies');
    }
}
