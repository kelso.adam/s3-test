<?php

namespace App\Behaviors;

trait Enumerable {
    protected static $enums = [];

    /**
     * Cache the reflection results
     *
     * @return mixed
     */
    protected static function Reflect()
    {
        if(empty(self::$enums)) {
            try {
                self::$enums = (new \ReflectionClass(self::class))
                    ->getConstants();
            }catch (\ReflectionException $e) {
                throw new \InvalidArgumentException($e->getMessage());
            }
        }
    }

    public static function all()
    {
        self::Reflect();

        return self::$enums;
    }

    public static function array()
    {
        self::Reflect();

        $result = [];

        foreach(self::$enums as $key => $value) {
            $result[] = ['label' => $key, 'value' => $value];
        }

        return $result;
    }

    public static function keys()
    {
        self::Reflect();

        return array_keys(self::$enums);
    }

    public static function values()
    {
        self::Reflect();

        return array_values(self::$enums);
    }

    public static function keyByValue($val)
    {
        self::Reflect();

        foreach(self::$enums as $key => $value)
        {
            if($value == $val)
            {
                return $key;
            }
        }
    }

    public static function valueByStringKey($key)
    {
        return constant(self::class.'::'.$key);
    }
}