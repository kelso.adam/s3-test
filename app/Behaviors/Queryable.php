<?php

namespace App\Behaviors;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait Queryable: This trait adds convenience methods for classes that allow for a lot of querying parameters for db calls.
 * The primary intended purpose if for resource controllers that allow users to request relations, order by fields, limits,
 * and so forth. However, the trait can really be used anywhere.
 *
 * @package app\Behaviors
 */
trait Queryable {
    /**
     * Require the method that determines what relationships the user can include or load.
     * @return array
     */
    abstract protected function queryableRelations();

    /**
     * The intended intro to this trait. Parses out the field keys and values and in turn
     * fires off the other included fields if they exist.
     * @param Builder|Model $query
     * @param array $fields
     * @return Builder
     */
    public function queryByFields(Builder $query, array $fields) {
        foreach($fields as $key => $value) {
            if(method_exists($this, $key)) {
                $query = $this->$key($query, $value);
            }
        }

        return $query;
    }

    /**
     * Load any additional relations requested in the index
     *
     * @param Builder      $query
     * @param array|string $includes
     *
     * @return Builder
     */
    protected function includes(Builder $query, $includes) {
        $includes = $this->stringToArray($includes);

        foreach($includes as $include) {
            // We check if the relation is specifically supported for the user.
            // If not, we ignore the "relation".
            if(in_array($include, $this->queryableRelations())) {
                $query->with($include);
            }
        }

        return $query;
    }

    /**
     * Load any requested relations from the DB that are related to the given model.
     * @param Model $model
     * @param string $relations
     *
     * @return Model
     */
    protected function loadRelations(Model $model, $relations) {
        $relations = $this->stringToArray($relations);

        foreach($relations as $relation) {
            // We check if the relation is specifically supported for the user.
            // If not, we ignore the "relation".
            if(in_array($relation, $this->queryableRelations())) {
                $model->load($relation);
            }
        }

        return $model;
    }

    /**
     * Set an order of results that should be returned from the query.
     * @param Builder $query
     * @param string  $order
     *
     * @return Builder
     */
    protected function order(Builder $query, $order) {
        return $query->orderBy($order);
    }

    /**
     * Set a descending order of the results that should be returned from the query.
     * @param Builder $query
     * @param         $order
     *
     * @return Builder
     */
    protected function orderDesc(Builder $query, $order) {
        return $query->orderByDesc($order);
    }

    /**
     * Set a limit of results that should be returned from the query.
     * @param Builder $query
     * @param int  $limit
     *
     * @return Builder
     */
    protected function limit(Builder $query, $limit) {
        return $query->limit($limit);
    }

    /**
     * Set a number of records to skip before finding records to return.
     * @param Builder $query
     * @param         $skip
     * @return Builder
     */
    protected function skip(Builder $query, $skip) {
        return $query->skip($skip);
    }

    /**
     * Split the string on commas, or return a single index array if no commas exist.
     * @param string $arg
     *
     * @return array
     */
    protected function stringToArray($arg) {
        if(is_array($arg)) {
            return $arg;
        }

        if(strpos($arg, ',') !== -1) {
            return explode(',', $arg);
        }

        return [$arg];
    }
}
