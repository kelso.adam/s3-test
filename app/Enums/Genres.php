<?php

namespace App\Enums;


use App\Behaviors\Enumerable;

class Genres
{
    use Enumerable;

    const Action = 1;
    const Adventure = 2;
    const Buddy = 8;
    const Cartoon = 14;
    const Comedy = 3;
    const ComingOfAge = 9;
    const Crime = 4;
    const Fantasy = 5;
    const Horror = 10;
    const Mystery = 6;
    const PostApocalypse = 12;
    const Romance = 11;
    const Thriller = 7;
    const Tragedy = 14;
    const Zombie = 13;
}