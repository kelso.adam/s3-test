<?php

namespace App\Http\Controllers\Api;

use App\Behaviors\Queryable;
use App\Http\Requests\MovieRequest;
use App\Http\Controllers\Controller;
use App\Entities\Movie;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Response;

class MovieController extends Controller
{
    use Queryable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = $this->queryByFields(Movie::query(), request()->all());

        return response(['data' => $query->get()]);
    }

    public function years()
    {
        $years = Movie::select('year');

        if(request()->has('year')) {
            $years = $years->where('year', 'like', request('year').'%');
        }

        $years = $years->groupBy('year')
            ->pluck('year');
        return response(['data' => array_map(function($item){
            return ['label' => (string)$item, 'value' => (string)$item];
        }, $years->toArray())]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\MovieRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MovieRequest $request)
    {
        $movie = $this->build($request, new Movie());
        $movie->user_id = $request->user()->id;
        $movie->save();

        $movie->actors()->sync($request->get('actors'));

        return response(['data' => $movie], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response([
            'data' => Movie::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\MovieRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MovieRequest $request, $id)
    {
        $movie = $request->user()->movies()->findOrFail($id);
        $movie = $this->build($request, $movie);

        if($movie->isDirty()) {
            $movie->save();
        }

        $movie->actors()->sync($request->get('actors'));

        return response(['data' => $movie]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movie = request()->user()->movies()->whereId($id)->first();

        if(!is_null($movie)) {
            $movie->delete();
        }

        // We don't tell a user if they did not delete a movie they did not create.
        // It's a silent refusal by design.
        return response(null, Response::HTTP_NO_CONTENT);
    }

    protected function build(MovieRequest $request, Movie $movie)
    {
        $movie->title = $request->get('title', $movie->title);
        $movie->year = $request->get('year', $movie->year);
        $movie->rating = $request->get('rating', $movie->rating);
        $movie->genre = $request->get('genre', $movie->genre);

        return $movie;
    }

    /**
     * Return the relations that are allowed to be queries on the movie object.
     *
     * @return array
     */
    protected function queryableRelations()
    {
        return ['actors', 'user'];
    }

    /**
     * The query builder logic to search movies by actors appearing in it.
     *
     * @param Builder $query
     * @param string $value
     * @return Builder
     */
    protected function actors($query, $value)
    {
        return $query->whereHas('actors', function($q) use ($value) {
            return $q->where('actor_id', $value);
        });
    }

    protected function user($query, $value)
    {
        return $query->whereUserId($value);
    }

    protected function year($query, $value)
    {
        return $query->where('year', '=', $value);
    }

    protected function genre($query, $value)
    {
        return $query->whereGenre($value);
    }

    protected function rating($query, $value)
    {
        return $query->whereRating($value);
    }
}
