<?php

namespace App\Http\Controllers\Api;

use App\Entities\Actor;
use App\Http\Controllers\Controller;

class ActorsController extends Controller
{
    public function index()
    {
        $query = Actor::query();

        if(request()->has('name')) {
            $query = $query
                ->where('first_name', 'like', '%'.request('name').'%')
                ->orWhere('last_name', 'like', '%'.request('name').'%');
        }

        return response([
            'data' => $query->orderBy('last_name')->get()
        ]);
    }

    public function show($id)
    {
        return response([
            'data' => Actor::findOrFail($id)
        ]);
    }
}
