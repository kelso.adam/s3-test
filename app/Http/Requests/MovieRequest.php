<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // We're allowing all through, because routing should block the controller
        // with middleware.
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'year' => 'required|digits:4',
            'genre' => 'integer',
            'rating' => 'integer|between:1,5',
            'actors.*' => 'integer|exists:actors,id'
        ];
    }
}
