# S3 Coding Test

> This code package is Adam Kelso's answer to the request for a sample project to solve. As the framework choice was optional, Laravel was used. Furthermore, this project was created with the intention to show as real of a working app as possible because creating a single CRUD based class is no use to anyone when placed in the context of a large-scale app.

## Setting Up and Installing

To install this package, move all files into a directory from which to serve as a site. Make sure to either set up a virtual host or some other equivalent, depending on your system.

1. Run `composer install` from the root directory.
1. Copy and paste the file **.env.example** to **.env** and fill in the necessary database connection info. You will also need to update the `APP_URL` variable to reflect what you set as your domain for the site server.
1. Run `php artisan key:generate` to create the app's hashing key.
1. Run `php artisan migrate` to create the necessary database tables.
1. Run `php artisan db:seed` to fill in the database with necessary, fake data.
1. Run `npm install` to install all necessary node modules.
1. Run `npm run dev` or `npm run production` to build the front-end assets (CSS, JS, etc.) used in the app.

Once the previous commands have all completed, the app should be ready to run.

## Choice Explanations

### Laravel
The prompt specified that the backend could optionally use a framework. With that option given, my first choice for a PHP framework would almost always be Laravel. Since I wanted to show all of my decision making processes for this project, it only made sense to continue with it.

### VueJS / Bulma
I have a lot of recent experience with both VueJS and Bulma (a CSS Framework).

### Laravel Passport
Because Laravel's Passport package natively supports multiple versions of the OAuth2 spec, I've defaulted to using OAuth for all API's I make on the framework for more than a year. There is no reason not to use OAuth since it is so easy to add to the framework at this point. Furthermore, all my apps that are SPAs generally consume the same API I could expose to partners with next to no effort.

## Queryable

One aspect of this code sample that is probably of note will be the trait `Queryable`, which can easily be placed on any class and allow a user or client making requests to the app to specify their own query syntax directly in the URL. The trait checks the request URL for ORM related items to either nest inside the results objects and also any query constraints. The trait provides several common methods like `limit`, `order`, and `skip`. However, if there are any specialized fields the client should be able to query by, a method with the same name can always be added to the class the trait is placed on.

```
Example: GET request to http://app.url/api/movies?includes=actors&orderBy=year
```

The URL above would return a response that nests all actors of note appearing in the movie as a child array of the movie object. It would also order all movies by the year in which it was released.

```
Example: GET request to http://app.url/api/movies?actors=5,27
```

The URL above would return a response of movies in which either the actors with IDs of either 5 or 27 have appeared in.

```
Example: GET request to http://app.url/api/movies?actors=5,27&rating=5
```

The URL above would return a response similar to the request before except would only include results that have a 5-star rating.

## Tests

A full suite of integration tests have been written to ensure code quality. To run the test suit, run `./vendor/bin/phpunit` from the command line in the root of the project.