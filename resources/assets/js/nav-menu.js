;(function() {
    "use strict";
    document.querySelectorAll('.navbar-burger')
        .forEach(function(item) {
            item.addEventListener('click', (e) => {
                let target = document.querySelector('#' + item.getAttribute('data-target'))
                if(target.classList.contains('navbar-menu')) {
                    target.classList.toggle('is-active');
                }
                item.classList.toggle('is-active');
            });
        });
})();