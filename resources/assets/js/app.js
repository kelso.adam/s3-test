import Vue from 'vue';
import VueRouter from 'vue-router';
import Movies from './pages/Movies';
import MovieForm from './pages/MovieForm';
import axios from 'axios';

axios.defaults.headers.common = {
    'X-CSRF-TOKEN'    : window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

Vue.use(VueRouter);

const routes = [
    {path: '/movies', component: Movies, name: 'Movies', props: (route) => ({
        year:route.query.year,
        genre:route.query.genre,
        actors:route.query.actors,
        rating:route.query.rating
    })},
    {path: '/movies/:id', component: MovieForm, name: 'MovieForm', props: (route) => ({
            id:route.params.id
        })}
];

const router = new VueRouter({
    mode: 'history',
    base: '/',
    routes: routes
});

window.app = new Vue({
    router
}).$mount('#wrapper');