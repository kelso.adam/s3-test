import axios from 'axios';
import { Errors } from './errors';

export class Form {
	constructor(data) {
		this.originalData = data;
		this.loading = false;

		for(let field in data) {
			this[field] = data[field];
		}

		this.errors = new Errors();
	}

	all() {
		let data = {};

		for(let property in this.originalData) {
			data[property] = this[property];
		}

		return data;
	}

	post(url) {
		return this.submit('post', url);
	}

	put(url) {
		return this.submit('put', url);
	}

	submit(requestType, url) {
		let that = this;
		this.loading = true;
		return new Promise((resolve, reject) =>
			axios[requestType](url, this.all())
				.then(response => {
					resolve(response.data);
					that.loading = false;
				})
				.catch(error => {
					if(error.response.status === 422) {
						that.onFail(error.response.data.errors);
					} else {
						reject(error.response);
					}
					that.loading = false;
				})
		);
	}

	onFail(errors) {
		this.errors.set(errors);
	}
}

