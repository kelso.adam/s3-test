import axios from 'axios';

export default {
    queryable(id) {
        let q = {
            label:'Actor',
            value:'actors',
            options(query) {
                return axios.get(route('actors.index') + '?name=' + query)
                    .then((response) => response.data.data.map((item) => {
                        item.label = item.first_name + ' ' + item.last_name;
                        return item;
                    }));
            }
        };

        if(id) {
            q.initial = axios.get(route('actors.show', {id:id}))
                .then(resp => {
                    let item = resp.data.data;
                    item.label = item.first_name + ' ' + item.last_name;
                    return item;
                })
        }

        return q;
    }
}