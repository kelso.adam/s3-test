import axios from 'axios';

export default {
    queryable(value) {
        let q = {
            label:'Year',
            value:'year',
            options(query) {
                return axios.get(route('movies.years') + '?year=' + query)
                    .then((response) => response.data.data)
            }
        };

        if(value) {
            q.initial = new Promise((resolve, reject) => {
                resolve({label: value + '', id: value})
            });
        }

        return q;
    }
}