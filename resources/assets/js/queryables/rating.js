export default {
    ratings: [
        {label:'1', value:1},
        {label:'2', value:2},
        {label:'3', value:3},
        {label:'4', value:4},
        {label:'5', value:5}
    ],
    queryable(id) {
        let q = {
            label:'Rating',
            value:'rating',
            options(query) {
                return new Promise((resolve, reject) => {
                    resolve(this.ratings)
                })
            }
        };

        if(id) {
            q.initial = new Promise((resolve, reject) => {
               resolve(this.ratings.find(rat => rat.value === id));
            });
        }

        return q;
    }
}