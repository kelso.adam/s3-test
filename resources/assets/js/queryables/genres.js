export default {
    queryable(id) {
        let q = {
            label:'Genre',
            value:'genre',
            options(query) {
                return new Promise((resolve, reject) => {
                    resolve(
                        window.genres.filter((item) => {
                            return item.label.toLowerCase().indexOf(query.toLowerCase()) !== -1;
                        })
                    )
                });
            }
        };

        if(id) {
            q.initial = new Promise((resolve, reject) => {
               resolve(
                   window.genres.find((item) => {
                       return item.value === id;
                   })
               )
            });
        }

        return q;
    }
}