@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="container">
            <h2 class="has-text-centered title">Please Login</h2>
            <div class="columns">
                <div class="column is-half-desktop is-offset-3-desktop">
                    <form method="post" action="/login">
                        {{ csrf_field() }}
                        <div class="field">
                            <label class="label" for="email">Your email</label>
                            <p class="control has-icon has-icon-right">
                                <input class="input @if($errors->has('email')) is-danger @endif" type="email" required id="email" name="email" value="{{ old('email') ?? 'demo@example.com' }}">
                            </p>
                            @include('partials.form-error-message', ['field' => 'email'])
                        </div>
                        <div class="field">
                            <label class="label" for="password">Password</label>
                            <p class="contorl has-icon has-icon-right">
                                <input class="input @if($errors->has('password')) is-danger @endif" type="password" name="password" id="password" required value="password" />
                            </p>
                            @include('partials.form-error-message', ['field' => 'password'])
                        </div>
                        <div class="field">
                            <label class="checkbox" for="remember">
                                <input value="yes" type="checkbox" id="remember" name="remember" />
                                &nbsp; Remember Me
                            </label>
                        </div>
                        <p class="has-text-centered">
                            <button type="submit" class="button is-primary is-fullwidth" name="submit"> &nbsp; Login &nbsp; </button>
                        </p>
                    </form>
                </div>
            </div>
            <br /><br />
            <p class="has-text-centered">
                <a href="{{ url('/password/reset') }}">Forgot Your Password?</a>
            </p>
        </div>
    </section>
@endsection
