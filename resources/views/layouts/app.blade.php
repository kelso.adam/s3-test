<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token()
        ]) !!};
    </script>

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <nav class="navbar" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
            <a class="navbar-item" href="{{ url('/') }}">
                {{--<img src="{{ asset('/images/logo.jpg') }}" alt="Logo" width="112" height="28">--}}
                <span class="badge">Logo</span>
            </a>

            <div class="navbar-burger" data-target="mainNav">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        @if(Auth::check())
            <div class="navbar-menu" id="mainNav">
                <div class="navbar-end">
                    <a class="navbar-item" href="{{ route('logout') }}">
                        Logout
                    </a>
                </div>
            </div>
        @endif
    </nav>

    <section class="container is-fluid">
        @yield('content')
    </section>

    <!-- Base navigation toggling for mobile -->
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/nav-menu.js') }}"></script>

    @if(Auth::check())
        @routes
        <script>window.genres = {!! json_encode(\App\Enums\Genres::array()) !!};</script>

        <!-- Scripts -->
        <script src="{{ mix('js/vendor.js') }}"></script>
        <script src="{{ mix('js/app.js') }}"></script>

        @if(Session::has('url.intended'))
            <script>app.$router.push({ path: '{!! Session::get('url.intended') !!}' });</script>
        @else
            <script>app.$router.push({ name:'Movies' });</script>
        @endif
    @endif
</body>
</html>
