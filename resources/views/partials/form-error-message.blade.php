@if($errors->has($field))
    <p class="help is-danger">{{ implode('<br />', $errors->get($field)) }}</p>
@endif