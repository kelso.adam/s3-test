@extends('layouts.app')

@section('content')
<div id="wrapper">
    <router-view></router-view>
</div>
@endsection
