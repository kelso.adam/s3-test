<?php

namespace Tests\Feature\Api;

use App\Entities\Actor;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class Actors extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

   public function test_will_return_all_actors_if_no_query()
   {
       factory(Actor::class)->times(25)->create();

       $this->get(route('actors.index'))
           ->assertStatus(200)
           ->assertJsonCount(25, 'data');
   }

   public function test_will_find_matches_based_on_name()
   {
       factory(Actor::class)->times(6)->create(['first_name' => 'test owiejwij']);
       factory(Actor::class)->times(3)->create(['last_name' => 'awoiejwoi test aowiej']);
       factory(Actor::class)->times(40)->create();

       $this->get(route('actors.index').'?name=test')
           ->assertStatus(200)
           ->assertJsonCount(9, 'data');
   }
}
