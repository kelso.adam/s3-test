<?php

namespace Tests\Feature\Api;

use App\Entities\{
    Movie, User, Actor
};
use App\Enums\Genres;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class MovieCRUDTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    public  $user,
            $ajax = ['Accept' => 'application/json'];

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        Passport::actingAs($this->user);
    }

    public function test_validation_is_required_to_create_and_update_movies()
    {
        $movie = factory(Movie::class)->create(['user_id' => $this->user->id]);

        $this->post(route('movies.store'), ['title' => ''], $this->ajax)
            ->assertStatus(422);

        $this->put(route('movies.update', $movie->id), ['title' => ''], $this->ajax)
            ->assertStatus(422);
    }

    public function test_user_can_create_new_movie()
    {
        $data = [
            'title' => 'Test Movie',
            'year' => 2018
        ];
        $this->assertDatabaseMissing('movies', $data);

        $this->post(route('movies.store'), $data, $this->ajax)
            ->assertStatus(201)
            ->assertJsonFragment($data);

        $data['user_id'] = $this->user->id;

        $this->assertDatabaseHas('movies', $data);
    }

    public function test_user_can_update_a_movie_they_created()
    {
        $movie = factory(Movie::class)->create(['user_id' => $this->user->id]);

        $this->put(route('movies.update', $movie->id), [
            'title' => 'Reset title',
            'year' => $movie->year
        ], $this->ajax)
            ->assertStatus(200)
            ->assertJsonFragment(['title' => 'Reset title']);

        $this->assertDatabaseHas('movies', [
            'title' => 'Reset title',
            'year' => $movie->year,
            'user_id' => $this->user->id
        ]);
    }

    public function test_a_user_cannot_update_a_movie_they_did_not_create()
    {
        $movie = factory(Movie::class)->create(['user_id' => $this->user->id + 1]);

        $this->put(route('movies.update', $movie->id), [
            'title' => 'Anything',
            'year' => $movie->year
        ], $this->ajax)
            ->assertStatus(404);
    }

    public function test_a_user_can_view_any_movie()
    {
        $movie1 = factory(Movie::class)->create(['user_id' => $this->user->id]);
        $movie2 = factory(Movie::class)->create();

        $this->get(route('movies.show', $movie1->id))
            ->assertStatus(200)
            ->assertJsonFragment(['title' => $movie1->title]);

        $this->get(route('movies.show', $movie2->id))
            ->assertStatus(200)
            ->assertJsonFragment(['title' => $movie2->title, 'modifiable' => false]);
    }

    public function test_a_user_can_only_delete_movies_they_created()
    {
        $movie1 = factory(Movie::class)->create(['user_id' => $this->user->id]);
        $movie2 = factory(Movie::class)->create();

        $this->delete(route('movies.destroy', $movie1->id))
            ->assertStatus(204);

        $this->assertDatabaseMissing('movies', ['id' => $movie1->id]);

        $this->delete(route('movies.destroy', $movie2->id))
            ->assertStatus(204);

        $this->assertDatabaseHas('movies', ['id' => $movie2->id]);
    }

    public function test_can_query_movies_by_user_that_created()
    {
        factory(Movie::class)->times(3)->create(['user_id' => $this->user->id]);
        factory(Movie::class)->times(20)->create();

        $this->get(route('movies.index').'?user='.$this->user->id)
            ->assertStatus(200)
            ->assertJsonCount(3, 'data');
    }

    public function test_can_query_movies_by_year()
    {
        factory(Movie::class)->times(4)->create(['year' => 2004]);
        factory(Movie::class)->times(20)->create(['year' => 2010]);

        $this->get(route('movies.index').'?year=2004')
            ->assertStatus(200)
            ->assertJsonCount(4, 'data');
    }

    public function test_can_query_movies_by_genre()
    {
        factory(Movie::class)->times(7)->create(['genre' => Genres::Comedy]);
        factory(Movie::class)->times(20)->create(['genre' => Genres::Action]);

        $this->get(route('movies.index').'?genre='.Genres::Comedy)
            ->assertStatus(200)
            ->assertJsonCount(7, 'data');
    }

    public function test_can_query_movies_by_actors()
    {
        factory(Movie::class)->times(30)->create();
        $movie = factory(Movie::class)->create();
        $actor = factory(Actor::class)->create();

        $movie->actors()->attach($actor);

        $this->get(route('movies.index').'?actors='.$actor->id)
            ->assertStatus(200)
            ->assertJsonCount(1, 'data');
    }
}
