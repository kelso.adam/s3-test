<?php

namespace Tests\Unit\Behaviors;

use App\Behaviors\Queryable;
use App\Entities\{
    Movie, User, Actor
};
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class QueryableTest extends TestCase
{
    use DatabaseTransactions, DatabaseMigrations, Queryable;

    public $user, $movie;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->be($this->user);
        $this->movie = factory(Movie::class)->create(['user_id' => $this->user->id]);
    }

    protected function queryableRelations() {
        return ['user', 'actors'];
    }

    public function testStringToArray() {
        $noSplit = $this->stringToArray('somestring');
        $withSplit = $this->stringToArray('one,two,three');

        $this->assertTrue(is_array($noSplit));
        $this->assertTrue(is_array($withSplit));
        $this->assertEquals(['somestring'], $noSplit);
        $this->assertEquals(['one', 'two', 'three'], $withSplit);
    }

    public function testLoadOneRelation() {
        // First assert the relation is only lazy loaded
        $this->assertEmpty($this->movie->getRelations());

        // Now do the querying and assertions
        $this->loadRelations($this->movie, 'user');
        $this->assertArrayHasKey('user', $this->movie->getRelations());
        $this->assertInstanceOf(User::class, $this->movie->user);
    }

    public function testLoadManyRelations() {
        $this->assertEmpty($this->movie->getRelations());
        $this->loadRelations($this->movie, 'user,actors');
        $this->assertArrayHasKey('user', $this->movie->getRelations());
        $this->assertArrayHasKey('actors', $this->movie->getRelations());
    }

    public function testIgnoreLoadRelationsThatDontExist() {
        $this->loadRelations($this->movie, 'doesntExist,norDoesThis');
        $this->assertEmpty($this->movie->getRelations());
    }

    public function testSingleInclude() {
        $query = $this->includes(Movie::query(), 'user');
        $first = $query->where('id', $this->movie->id)->first();

        $this->assertArrayHasKey('user', $first->getRelations());
    }

    public function testMultipleIncludes() {
        $this->movie->actors()->saveMany(
            factory(Actor::class)->times(3)->make()
        );

        $query = $this->includes(Movie::query(), 'user,actors');
        $first = $query->where('id', $this->movie->id)->first();

        $this->assertArrayHasKey('user', $first->getRelations());
        $this->assertArrayHasKey('actors', $first->getRelations());
        $this->assertCount(3, $first->getRelations()['actors']);
    }

    public function testIgnoreIncludesThatDontExist() {
        $query = $this->includes(Movie::query(), 'user,doesntExist');
        $first = $query->where('id', $this->movie->id)->first();

        $this->assertArrayHasKey('user', $first->getRelations());
        $this->assertArrayNotHasKey('doesntExist', $first->getRelations());
    }

    public function testQueryByFields() {
        factory(Movie::class)->times(20)->create()->each(function(Movie $m) {
            $m->actors()->saveMany(
              factory(Actor::class)->times(rand(2, 5))->make()
            );
        });

        $result = $this->queryByFields(Movie::query(), [
            'includes' => 'user,actors,doesntexist',
            'limit' => 4,
            'order' => 'year',
            'another' => 'some method that doesnt exist'
        ])->get();

        $this->assertEquals(4, $result->count());
        $result->each(function($record) {
            $this->assertArrayHasKey('user', $record->getRelations());
            $this->assertArrayHasKey('actors', $record->getRelations());
            $this->assertArrayNotHasKey('doesntexist', $record->getRelations());
        });
    }
}
