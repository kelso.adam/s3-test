<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api'], 'namespace' => 'Api'], function () {
    Route::get('/movies/years', 'MovieController@years')->name('movies.years');
    Route::apiResource('/movies', 'MovieController');
    Route::get('/actors', 'ActorsController@index')->name('actors.index');
    Route::get('/actors/{id}', 'ActorsController@show')->name('actors.show');
});
