<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');

// Deep linking "catch all" route
// This route is to check if a user is copying and pasting a URL to a new browser or tab.
Route::fallback(function($catchall, \Illuminate\Http\Request $request) {
    $routes = array_map(function($route) {
        return $route->uri;
    }, Route::getRoutes()->getRoutes());

    $segments = explode('/', $catchall);
    $caughtAll = $catchall;
    if(isset($segments[1]) && is_numeric($segments[1])) {
        $caughtAll = $segments[0].'/{'.str_singular($segments[0]).'}';
    }

    if(in_array('api/'.$caughtAll, $routes) || in_array('ajax/'.$caughtAll, $routes)) {
        // If the user is logged in, redirect straight to the app
        if($request->user() !== null) {
            return redirect()->route('home')->with('url.intended', $catchall.setQueryIfExists($request));
        }

        // If the user is not logged in, send them to the login form, but keep their
        // requested url so Laravel will auto redirect them after login.
        $request->session()->put('url.intended', $catchall.setQueryIfExists($request));
        return redirect(route('login'));
    }

    // If no match, then we can't help.
    throw new Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
});