<?php

use Illuminate\Database\Seeder;
use App\Entities\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            'name' => 'Demo Account',
            'password' => bcrypt('password'),
            'email' => 'demo@example.com'
        ]);

        factory(User::class)->times(25)->create();
    }
}
