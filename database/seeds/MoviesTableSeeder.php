<?php

use Illuminate\Database\Seeder;
use App\Entities\{
    Movie, Actor
};

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('actors_movies')->truncate();

        Movie::truncate();
        Actor::truncate();

        $user = \App\Entities\User::whereEmail('demo@example.com')->first();
        factory(Movie::class)
            ->times(4)
            ->create(['user_id' => $user->id])
            ->each(function(Movie $movie) {
                $actor = factory(Actor::class)
                    ->times(rand(1,3))
                    ->create();

                $movie->actors()->attach($actor->pluck('id'));
            });

        factory(Movie::class)
            ->times(25)
            ->create()
            ->each(function(Movie $movie) {
                $actors = factory(Actor::class)
                    ->times(rand(1, 5))
                    ->create();

                $movie->actors()->attach($actors->pluck('id'));
            });
    }
}
