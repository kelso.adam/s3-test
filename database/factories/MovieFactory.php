<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Movie::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(\App\Entities\User::class)->create()->id;
        },
        'title' => $faker->words(rand(1, 6), true),
        'year' => $faker->year,
        'rating' => $faker->numberBetween(1, 5),
        'genre' => $faker->randomElement(\App\Enums\Genres::values())
    ];
});
