<?php

if(!function_exists('setQueryIfExists')) {
    function setQueryIfExists(\Illuminate\Http\Request $request) {
        return (is_null($request->getQueryString()))
            ? ''
            : '?'.$request->getQueryString();
    }
}